#define PY_SSIZE_T_CLEAN
#include <Python.h>

static PyObject *test(PyObject *self, PyObject *args)
{
    if (!PyArg_ParseTuple(args, ""))
        return NULL;

    return PyUnicode_FromString("test");
}

static PyMethodDef methods[] = {
    {"test", test, METH_VARARGS, "A test function."},
    {NULL, NULL, 0, NULL}
};

static struct PyModuleDef module = {
    PyModuleDef_HEAD_INIT,
    .m_name = "_extension",
    .m_doc = "A simple test extension",
    .m_size = -1,
    .m_methods = methods,
};

PyMODINIT_FUNC PyInit_extension(void)
{
    return PyModule_Create(&module);
}
